package com.aviava.FizzBuzz.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.ws.rs.QueryParam;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.aviava.FizzBuzz.error.LimitExceedException;

@RestController
public class FizzBuzzController extends ResponseEntityExceptionHandler{
	
	
	
	@GetMapping(path= "/fizz")
	
	public @ResponseBody ResponseEntity<Object> getFizzBuZZ(@QueryParam(value = "limit") @Min(1) @Max(1000) Integer limit,
			@QueryParam(value = "next") Integer next, @QueryParam(value = "previous") Integer previous) {
		
		// check input with in the range or not
		if(limit >= 1 && limit <= 1000) {
			//check today is wednesday or not
			Calendar calendar = Calendar.getInstance();
			boolean isWednesDay = calendar.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY ? true : false;
			//If Wednesday modify Fizz and Buzz values
			String fizz = isWednesDay ? "Wizz" : "Fizz";
			String buzz = isWednesDay ? "Wuzz" : "Buzz";
			//if next is greater than previous calculate start position otherwise start from 0
			int start = (next > previous ? previous * 20 : 0 *20) + 1;
			//if start is greater than limit start from 0
			start = start <= limit ? start : 0;
			//if end is greater than limit assign limit to end
			int end =  start + 20 < limit ? start + 20 : limit;
			List<String> numbersList = new ArrayList<String>();
			for(int i = start ; i < end ; i++) {
				if(i % 3 == 0 && i % 5 == 0) {
					
					numbersList.add(new StringBuilder().append(fizz).append(" ").append(buzz).toString());
				}else if(i % 3 == 0 ) {
					numbersList.add(fizz);
					
				} else if(i % 5 == 0 ) {
					numbersList.add(buzz);
				} else {
					numbersList.add(""+i);
				}
			}
			return ResponseEntity.ok(numbersList);
		} else {
			throw new LimitExceedException(new Exception("Limit is not in range"));
		}
		
	}
	
}
