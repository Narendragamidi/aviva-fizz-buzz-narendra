package com.aviava.FizzBuzz.controller;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.aviava.FizzBuzz.error.ErrorDetails;
import com.aviava.FizzBuzz.error.LimitExceedException;

@RestController

@ControllerAdvice
public class LimitsExceedExceptionHandler extends ResponseEntityExceptionHandler{
	
	@ExceptionHandler(LimitExceedException.class)
	  public final ResponseEntity<ErrorDetails> handleUserNotFoundException(LimitExceedException ex, WebRequest request) {
	    ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(),
	        request.getDescription(false));
	    return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	  }
}
