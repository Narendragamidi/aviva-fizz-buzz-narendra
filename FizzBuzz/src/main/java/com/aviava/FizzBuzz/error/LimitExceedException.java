package com.aviava.FizzBuzz.error;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class LimitExceedException extends RuntimeException{
	
	public LimitExceedException(Exception e) {
		super(e);
	}
}
